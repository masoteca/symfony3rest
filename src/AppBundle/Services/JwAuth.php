<?php

namespace AppBundle\Services;

use Firebase\JWT\JWT;

/**
 * Description of JwAuth
 *
 * @author Sistemas
 */
class JwAuth {

    //put your code here
    public $manager;
    public $key = 'misecretakeydeprueba12342141';

    public function __construct($manager) {
        $this->manager = $manager;
    }

    public function singup($email, $password, $getHash = null) {
        $user = $this->manager->getRepository('BackendBundle:User')->findOneby(array(
            "email" => $email,
            "password" => $password
        ));
        $singup = false;
        if (is_object($user)) {
            $singup = true;
        }
        if (true == $singup) {
            //generar token JWT

            $token = array(
                "sub" => $user->getId(),
                "email" => $user->getEmail(),
                "name" => $user->getName(),
                "surname" => $user->getSurname(),
                "role" => $user->getRole(),
                "iat" => time(),
                "exp" => time() + ( 2 * 60 )
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, array('HS256'));
            if ($getHash == NULL) {
                $data = $jwt;
            } else {
                $data = $decoded;
            }
        } else {
            $data = array(
                "status" => "error",
                "data" => "fallido el login"
            );
        }

        return $data;
    }

    public function checktoken($jwt, $getIdentity = null) {
        $auth = false;
        try {
            $decoded = JWT::decode($jwt, $this->key, array('HS256'));
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e){
            $auth = false;
        }
        if(isset($decoded) && is_object($decoded) && isset($decoded)){
            $auth=true;
        }else{
            $auth = false;
        }
        if($getIdentity ==FALSE){
            return $auth;
        }else{
            return $decoded;
        }
    }

}